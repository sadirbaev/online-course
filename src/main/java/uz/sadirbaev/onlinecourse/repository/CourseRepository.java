package uz.sadirbaev.onlinecourse.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import uz.sadirbaev.onlinecourse.entity.Category;
import uz.sadirbaev.onlinecourse.entity.Course;

import java.util.List;
import java.util.Optional;

@Repository
public interface CourseRepository extends JpaRepository<Course, Integer> {
    List<Course> findCoursesByCategory(Category category);
    Optional<Course> findCoursesByName(String name);
}
