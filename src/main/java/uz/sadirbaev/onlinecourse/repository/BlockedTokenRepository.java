package uz.sadirbaev.onlinecourse.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import uz.sadirbaev.onlinecourse.entity.BlockedToken;

import java.util.List;

@Repository
public interface BlockedTokenRepository extends JpaRepository<BlockedToken, Integer> {

    List<BlockedToken> findBlockedTokenByToken(String token);
}
