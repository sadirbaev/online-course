package uz.sadirbaev.onlinecourse.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.sadirbaev.onlinecourse.entity.ERole;
import uz.sadirbaev.onlinecourse.entity.Role;
import uz.sadirbaev.onlinecourse.entity.User;

import java.util.Optional;

public interface RoleRepository extends JpaRepository<Role, Integer> {
    Optional<Role> findByName(ERole role);
}
