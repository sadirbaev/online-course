package uz.sadirbaev.onlinecourse.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import uz.sadirbaev.onlinecourse.entity.Category;
import uz.sadirbaev.onlinecourse.entity.Course;

import java.util.Optional;

@Repository
public interface CategoryRepository extends JpaRepository<Category, Integer> {
    Optional<Category> findCategoryByCourses(Course course);
}
