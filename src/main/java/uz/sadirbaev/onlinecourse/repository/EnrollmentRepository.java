package uz.sadirbaev.onlinecourse.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import uz.sadirbaev.onlinecourse.entity.Category;
import uz.sadirbaev.onlinecourse.entity.Course;
import uz.sadirbaev.onlinecourse.entity.Enrollment;
import uz.sadirbaev.onlinecourse.entity.User;

import java.util.List;

@Repository
public interface EnrollmentRepository extends JpaRepository<Enrollment, Integer> {

    List<Enrollment> findEnrollmentsByCourse(Course course);
}
