package uz.sadirbaev.onlinecourse.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import uz.sadirbaev.onlinecourse.entity.User;

import java.util.List;
import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<User, Integer> {
    Optional<User> findByUsername(String username);

    @Query(
            value = "SELECT u.id, u.username, u.password FROM users u inner join user_roles ur on u.id=ur.users_id inner join roles r on ur.role_id=r.id where r.name='ROLE_TEACHER'",
            nativeQuery = true)
    List<User> findAllTeachers();
}
