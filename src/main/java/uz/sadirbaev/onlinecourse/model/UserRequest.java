package uz.sadirbaev.onlinecourse.model;

import java.io.Serializable;

public class UserRequest implements Serializable {

    private String username;
    private String password;
    private Integer roleId;

    public UserRequest() {
    }

    public UserRequest(String username, String password, Integer roleId) {
        this.username = username;
        this.password = password;
        this.roleId = roleId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Integer getRoleId() {
        return roleId;
    }

    public void setRoleId(Integer roleId) {
        this.roleId = roleId;
    }
}
