package uz.sadirbaev.onlinecourse.model;

import java.io.Serializable;

public class CategoryRequest implements Serializable {

    private String name;

    public CategoryRequest() {
    }

    public CategoryRequest(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
