package uz.sadirbaev.onlinecourse.model;

import java.io.Serializable;

public class EnrollmentRequest implements Serializable {

    private Integer userId;
    private Integer courseId;

    public EnrollmentRequest() {
    }

    public EnrollmentRequest(Integer userId, Integer courseId) {
        this.userId = userId;
        this.courseId = courseId;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getCourseId() {
        return courseId;
    }

    public void setCourseId(Integer courseId) {
        this.courseId = courseId;
    }
}
