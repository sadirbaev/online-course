package uz.sadirbaev.onlinecourse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import uz.sadirbaev.onlinecourse.entity.ERole;
import uz.sadirbaev.onlinecourse.entity.Role;
import uz.sadirbaev.onlinecourse.entity.User;
import uz.sadirbaev.onlinecourse.repository.RoleRepository;
import uz.sadirbaev.onlinecourse.repository.UserRepository;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

@SpringBootApplication
public class OnlinecourseApplication implements CommandLineRunner {

	@Autowired
	UserRepository userRepository;

	@Autowired
	RoleRepository roleRepository;

	public static void main(String[] args) {
		SpringApplication.run(OnlinecourseApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
//		List<User> userEntityList = userRepository.findAll();
//		registerAdmin();
	}

	void registerAdmin(){
		Role role = roleRepository.findByName(ERole.ROLE_ADMIN).orElseThrow(() -> new RuntimeException("Error: Role is not found."));
		Set<Role> roleSet = new HashSet<>();
		roleSet.add(role);
		BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
		User user = new User("sadirbaev", encoder.encode("password"));
		user.setRoles(roleSet);
		System.out.println(user);
		userRepository.save(user);
	}
}
