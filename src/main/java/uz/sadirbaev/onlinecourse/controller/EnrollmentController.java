package uz.sadirbaev.onlinecourse.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import uz.sadirbaev.onlinecourse.entity.*;
import uz.sadirbaev.onlinecourse.model.CategoryRequest;
import uz.sadirbaev.onlinecourse.model.EnrollmentRequest;
import uz.sadirbaev.onlinecourse.repository.CategoryRepository;
import uz.sadirbaev.onlinecourse.repository.CourseRepository;
import uz.sadirbaev.onlinecourse.repository.EnrollmentRepository;
import uz.sadirbaev.onlinecourse.repository.UserRepository;

import java.util.List;

@RestController
public class EnrollmentController {

    @Autowired
    EnrollmentRepository enrollmentRepository;

    @Autowired
    CategoryRepository categoryRepository;

    @Autowired
    CourseRepository courseRepository;

    @Autowired
    UserRepository userRepository;


    @RequestMapping(value = "/add-enrollment", method = RequestMethod.POST)
    public ResponseEntity<?> addCourse(@RequestBody EnrollmentRequest enrollmentRequest) throws Exception {

        User user;
        try {
            user = userRepository.findById(enrollmentRequest.getUserId()).orElseThrow(() -> new RuntimeException("Error: User is not found."));
        } catch (RuntimeException e){
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
        }

        Course course;
        try {
            course = courseRepository.findById(enrollmentRequest.getCourseId()).orElseThrow(() -> new RuntimeException("Error: Course is not found."));
        } catch (RuntimeException e){
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
        }
        Enrollment enrollment = new Enrollment();
        enrollment.setUser(user);
        enrollment.setCourse(course);
        enrollmentRepository.save(enrollment);
        return ResponseEntity.status(HttpStatus.CREATED).body("Enrollment successfully created");
    }

    @RequestMapping(value = "/enrollments", method = RequestMethod.GET)
    public ResponseEntity<?> getAllEnrollments() throws Exception {
        List<Enrollment> enrollmentList = enrollmentRepository.findAll();
        return ResponseEntity.ok(enrollmentList);
    }

    @RequestMapping(value = "/enrollments-by-course", method = RequestMethod.GET)
    @PreAuthorize("hasRole('TEACHER')")
    public ResponseEntity<?> getEnrollmentsByCourses(@RequestParam(name="course") Integer courseId) throws Exception {

        Course course;
        try {
            course = courseRepository.findById(courseId).orElseThrow(() -> new RuntimeException("Error: Course is not found."));
        } catch (RuntimeException e){
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(e.getMessage());
        }

        List<Enrollment> enrollmentList = enrollmentRepository.findEnrollmentsByCourse(course);
        return ResponseEntity.ok(enrollmentList);
    }
}
