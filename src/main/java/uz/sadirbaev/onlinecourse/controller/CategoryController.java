package uz.sadirbaev.onlinecourse.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import uz.sadirbaev.onlinecourse.entity.Category;
import uz.sadirbaev.onlinecourse.entity.Course;
import uz.sadirbaev.onlinecourse.entity.User;
import uz.sadirbaev.onlinecourse.model.CategoryRequest;
import uz.sadirbaev.onlinecourse.model.UserRequest;
import uz.sadirbaev.onlinecourse.repository.CategoryRepository;
import uz.sadirbaev.onlinecourse.repository.CourseRepository;

import java.util.List;

@RestController
public class CategoryController {

    @Autowired
    CategoryRepository categoryRepository;

    @Autowired
    CourseRepository courseRepository;

    @RequestMapping(value = "/add-category", method = RequestMethod.POST)
    @PreAuthorize("hasRole('TEACHER')")
    public ResponseEntity<?> addCourse(@RequestBody CategoryRequest categoryRequest) throws Exception {
        Category category = new Category();
        category.setName(categoryRequest.getName());
        categoryRepository.save(category);
        return ResponseEntity.status(HttpStatus.CREATED).body("Category successfully created");
    }

    @RequestMapping(value = "/categories", method = RequestMethod.GET)
    public ResponseEntity<?> getAllCategories() throws Exception {
        List<Category> categoryList = categoryRepository.findAll();
        return ResponseEntity.ok(categoryList);
    }

    @RequestMapping(value = "/category-by-course", method = RequestMethod.GET)
    public ResponseEntity<?> getAllCategories(@RequestParam(name="course") String courseName) throws Exception {

        Course course;
        try {
            course = courseRepository.findCoursesByName(courseName).orElseThrow(() -> new RuntimeException("Error: Course is not found."));
        } catch (RuntimeException e){
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(e.getMessage());
        }

        Category category;
        try {
            category = categoryRepository.findCategoryByCourses(course).orElseThrow(() -> new RuntimeException("Error: Category is not found."));
        } catch (RuntimeException e){
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(e.getMessage());
        }
        return ResponseEntity.ok(category);
    }
}
