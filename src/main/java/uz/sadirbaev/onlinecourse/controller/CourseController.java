package uz.sadirbaev.onlinecourse.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;
import uz.sadirbaev.onlinecourse.entity.*;
import uz.sadirbaev.onlinecourse.model.CourseRequest;
import uz.sadirbaev.onlinecourse.model.UserRequest;
import uz.sadirbaev.onlinecourse.repository.CategoryRepository;
import uz.sadirbaev.onlinecourse.repository.CourseRepository;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

@RestController
public class CourseController {

    @Autowired
    CourseRepository courseRepository;

    @Autowired
    CategoryRepository categoryRepository;

    @RequestMapping(value = "/add-course", method = RequestMethod.POST)
    @PreAuthorize("hasRole('TEACHER')")
    public ResponseEntity<?> addCourse(@RequestBody CourseRequest courseRequest) throws Exception {

        Category category;
        try {
            category = categoryRepository.findById(courseRequest.getCategoryId()).orElseThrow(() -> new RuntimeException("Error: Category is not found."));
        } catch (RuntimeException e){
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
        }
        Course course = new Course();
        course.setName(courseRequest.getName());
        course.setCategory(category);

        courseRepository.save(course);
        return ResponseEntity.status(HttpStatus.CREATED).body("Course successfully created");
    }

    @RequestMapping(value = "/courses", method = RequestMethod.GET)
    public ResponseEntity<?> getAllCources() throws Exception {
        List<Course> courseList = courseRepository.findAll();
        return ResponseEntity.ok(courseList);
    }

    @RequestMapping(value = "/courses-by-category", method = RequestMethod.GET)
    public ResponseEntity<?> getCoursesByCategory(@RequestParam(name="category") Integer categoryId) throws Exception {
        Category category;
        try {
            category = categoryRepository.findById(categoryId).orElseThrow(() -> new RuntimeException("Error: Category is not found."));
        } catch (RuntimeException e){
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(e.getMessage());
        }

        List<Course> courseList = courseRepository.findCoursesByCategory(category);
        return ResponseEntity.ok(courseList);
    }
}
