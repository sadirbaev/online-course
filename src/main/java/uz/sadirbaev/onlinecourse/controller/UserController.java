package uz.sadirbaev.onlinecourse.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import uz.sadirbaev.onlinecourse.entity.BlockedToken;
import uz.sadirbaev.onlinecourse.entity.ERole;
import uz.sadirbaev.onlinecourse.entity.Role;
import uz.sadirbaev.onlinecourse.entity.User;
import uz.sadirbaev.onlinecourse.model.ErrorResponse;
import uz.sadirbaev.onlinecourse.model.UserRequest;
import uz.sadirbaev.onlinecourse.repository.BlockedTokenRepository;
import uz.sadirbaev.onlinecourse.repository.RoleRepository;
import uz.sadirbaev.onlinecourse.repository.UserRepository;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@RestController
public class UserController {

    @Autowired
    UserRepository userRepository;

    @Autowired
    RoleRepository roleRepository;

    @Autowired
    BlockedTokenRepository blockedTokenRepository;

    @RequestMapping(value = "/users", method = RequestMethod.GET)
    public ResponseEntity<?> getAllUsers() throws Exception {
        List<User> userEntityList = userRepository.findAll();
        return ResponseEntity.ok(userEntityList);
    }

    @RequestMapping(value = "/teachers", method = RequestMethod.GET)
    public ResponseEntity<?> getAllTeachers() throws Exception {
        List<User> userEntityList = userRepository.findAllTeachers();
        return ResponseEntity.ok(userEntityList);
    }



    @RequestMapping(value = "/sign-up", method = RequestMethod.POST)
    public ResponseEntity<?> signUp(@RequestBody UserRequest userRequest) throws Exception {
        Role role;
        try {
            role = roleRepository.findById(userRequest.getRoleId()).orElseThrow(() -> new RuntimeException("Error: Role is not found."));
        } catch (RuntimeException e){
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
        }
        Set<Role> roleSet = new HashSet<>();
        roleSet.add(role);
        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
        User user = new User(userRequest.getUsername(), encoder.encode(userRequest.getPassword()));
        user.setRoles(roleSet);
        userRepository.save(user);
        return ResponseEntity.status(HttpStatus.CREATED).body("User successfully created");
    }

    @RequestMapping(value = "/sign-out", method = RequestMethod.GET)
    public ResponseEntity<?> signOut(@RequestHeader (name="Authorization") String token) throws Exception {
        System.out.println(token);
        BlockedToken blockedToken = new BlockedToken(token);
        blockedTokenRepository.save(blockedToken);
        return ResponseEntity.status(HttpStatus.ACCEPTED).body("User successfully signed out");
    }


}
